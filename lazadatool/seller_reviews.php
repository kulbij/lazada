<?php
	include dirname(__FILE__).'/include.php';
	global $configs,$db;	
	$seller_id 		= ( isset($_GET['id']) ) ? $_GET['id'] : 0;
	

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	
	$product_reviews  = array();
	if( count($errors) > 0 ){
		pr($errors);
	}else{
		$seller_reviews = $db->read('seller_reviews',$seller_id,'seller_id')->all();
		
	}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Scrap Tool</title>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
  <style type="text/css">
     .form-control {
     margin-left: 0px;
     }
     .page-header, .page-header small {
     color: white !important;
     }
     label {
     color: black;
     }
     div#div_result {
        height: 129px;
        overflow-x: hidden;
    }
    body{
      font-size: 15px;
    }
    .t_lable {
      width: 25%;
    }
  </style>
  <script src="assets/jquery.min.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
      <div class="col-lg-12">
         <h1 class="page-header" style="color: black !important;">
            Seller Reviews
         </h1>
      </div>
   </div>
	<div class="row">
      <div class="col-lg-12">
      	<?php
      		if( count( $seller_reviews ) > 0 ){
      			foreach ($seller_reviews as $seller_review) {
      				?>
      				<table class="table table-bordered">
			          <tbody>
			          		<tr>
								<td class="t_lable">Name</td>
								<td><?php echo $seller_review->name; ?></td>
							</tr>
							<tr>
								<td class="t_lable">Text</td>
								<td><?php echo $seller_review->text; ?></td>
							</tr>
							<tr>
								<td class="t_lable">Created</td>
								<td><?php echo $seller_review->created_at_string; ?></td>
							</tr>

							<tr>
								<td class="t_lable">Review Type</td>
								<td><?php echo $seller_review->review_type; ?></td>
							</tr>
			          </tbody>
			        </table>
      				<?php
      			}
      		}

      	?>
        
      </div>
   </div>
</div>
</body>
</html>