<!DOCTYPE html>
<html>
<head>
  <title>Scrap Tool</title>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
  <style type="text/css">
     .form-control {
     margin-left: 0px;
     }
     .page-header, .page-header small {
     color: white !important;
     }
     label {
     color: black;
     }
     div#div_result {
        height: 129px;
        overflow-x: hidden;
    }
    body{
      font-size: 15px;
    }
    .t_lable {
      width: 25%;
    }
  </style>
  <script src="assets/jquery.min.js"></script>
</head>
<body>





<div class="container">
   <script type="text/javascript">
      var ajax_url =  '<?php echo home_url();?>/ajax.php';
      var home_url =  '<?php echo home_url();?>/';
      jQuery( document ).ready( function($){
          var txt_process = jQuery('.txt_process');
          $('#start').on('click',function(e){
             $("html, body").animate({ scrollTop: "0" });
              e.preventDefault();
              var dta = $( '#import_form' ).serializeArray();
      
              $('#form_filedset').attr('disabled','disabled');
              $('#start').attr('disabled','disabled');
              $('#continue').attr('disabled','disabled');
              //$('#reset').attr('disabled','disabled');
              
             
               write_to_log( 'Getting Data... <br>' );
             
              $.ajax({
                  type: "POST",
                  url: ajax_url+'?action=tool_start',
                  data: dta,
                  dataType: "json"
              })
              .done(function( respon ) {
                   write_to_log( respon.msg );
                  if( respon.status == 1 ){
                    if( respon.action == 'tool_show_info' ){
                       tool_show_info(respon.seller_id,respon.action);
                    }
                  }else{
					   $('#form_filedset').attr('disabled',false);
					   $('#start').attr('disabled',false);
					   $('#continue').attr('disabled',false);
				  }
              });
          }); 

	      
         function tool_show_info(seller_id,action){ 
             var dta = {
                  'action'      : 'tool_show_info',
                  'seller_id'   : seller_id
              };

            $.ajax({
                type: "POST",
                url: ajax_url,
                data: dta,
                timeout: 60000,
                dataType: "json"
            })
            .done(function( respon ) {  
                write_to_log( respon.msg );
                if( respon.status == 1 ){
                    $('#seller-info tbody').html( respon.xhtml );
                    if( respon.action == 'tool_get_reviews' ){
                      tool_get_reviews(respon.seller_id,respon.action,1,0);
                    }

                }
            });
            
         }

         function tool_get_product_reviews( seller_id,action,review_page ){
            var dta = {
                'action'      : 'tool_get_product_reviews',
                'seller_id'   : seller_id,
                'review_page'   : review_page,
            };

            $.ajax({
                type: "POST",
                url: ajax_url,
                data: dta,
                timeout: 60000,
                dataType: "json"
            })
            .done(function( respon ) {  
                write_to_log( respon.msg );
                if( respon.status == 1 ){
                    if( respon.action == 'tool_get_product_reviews' ){
                       tool_get_product_reviews(respon.seller_id,respon.action,respon.review_page);
                    }
                    if( respon.action == 'tool_get_product_discount' ){
                       $('.product_reviews').html('<a target="_blank" href="'+home_url+'product_reviews.php?id='+respon.seller_id+'">View Product Reviews</a>');

                       tool_get_product_discount(respon.seller_id,respon.action,1);
                    }
                   
                }
            });
         }
         function tool_get_product_discount( seller_id,action,product_page ){
             var dta = {
                'action'      : 'tool_get_product_discount',
                'seller_id'   : seller_id,
                'product_page'   : product_page,
            };

            $.ajax({
                type: "POST",
                url: ajax_url,
                data: dta,
                timeout: 60000,
                dataType: "json"
            })
            .done(function( respon ) {  
                write_to_log( respon.msg );
                if( respon.status == 1 ){
                    if( respon.action == 'tool_get_product_discount' ){
                       tool_get_product_discount(respon.seller_id,respon.action,respon.product_page);
                    }
                    if( respon.action == 'tool_reshow_info' ){
                       tool_reshow_info(respon.seller_id,respon.action);
                    }
                   
                }
            });
         }
         function tool_reshow_info( seller_id,action ){
            var dta = {
                'action'      : 'tool_reshow_info',
                'seller_id'   : seller_id,
            };
            $.ajax({
                type: "POST",
                url: ajax_url,
                data: dta,
                timeout: 60000,
                dataType: "json"
            })
            .done(function( respon ) {  
                write_to_log( respon.msg );
                if( respon.status == 1 ){
                   
                   $('.number_discount').text( respon.data.total_product_discount);
                   $('.number_non_discount').text( respon.data.total_product_non_discount);

                   $('#form_filedset').attr('disabled',false);
                   $('#start').attr('disabled',false);
                   $('#continue').attr('disabled',false);

                   alert('All Done !');
                }
            });
         }
         function tool_get_reviews( seller_id,action,review_page,review_type ){
            var dta = {
                'action'      : 'tool_get_reviews',
                'seller_id'   : seller_id,
                'review_page'   : review_page,
                'review_type'   : review_type,
            };

            $.ajax({
                type: "POST",
                url: ajax_url,
                data: dta,
                timeout: 60000,
                dataType: "json"
            })
            .done(function( respon ) {  
                write_to_log( respon.msg );
                if( respon.status == 1 ){
                    if( respon.action == 'tool_get_reviews' ){
                       tool_get_reviews(respon.seller_id,respon.action,respon.review_page,respon.review_type);
                    }
                    
                    if( respon.action == 'tool_get_product_reviews' ){
                       $('.seller_reviews').html('<a target="_blank" href="'+home_url+'seller_reviews.php?id='+respon.seller_id+'">View Seller Reviews</a>');
                       tool_get_product_reviews(respon.seller_id,respon.action,1);
                    }
                }
            });
         }


         function write_to_log( msg ){
            msg+= '<br>';
            jQuery('#div_result').prepend( msg );
         }
      });
      
      
   </script>
   <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header" style="color: black !important;">
            Lazada Tool
         </h1>
      </div>
   </div>
   <!-- /.row -->
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Setup</h3>
            </div>
            <div class="panel-body">
               <form method="post" action="" id="import_form" role="form">
                  <input type="hidden" name="action" value="tool_start">
                  <fieldset id="form_filedset">
                     <div class="form-group">
                          <label>Store Url</label>
                          <input type="text" name="store_url" class="form-control">
                     </div>
                     <button id="start" type="submit" name="start" class="btn btn-default">GET DATA</button>
                     <button type="button" onclick="window.location.reload()" id="continue" class="btn btn-danger">CLEAN</button>
                  </fieldset>
               </form>
            </div>
         </div>
      </div>
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i><span class="txt_process"> Processing</span><span class="loading-ajax"></span></h3>
            </div>
            <div id="div_result" class="panel-body">
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12">
        <table class="table table-bordered" id="seller-info">
          <tbody>
            
          </tbody>
        </table>
      </div>
   </div>
</div>


</body>
</html>
