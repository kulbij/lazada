<?php 
include_once dirname(__FILE__).'/config.php';
include_once dirname(__FILE__).'/Db.php';
global $db,$configs;
$db = new Db( 'mysql', $configs['db_host'],$configs['db_name'],$configs['db_user'],$configs['db_pass'] );

function home_url(){
	return sprintf(
	    "%s://%s%s",
	    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
	    $_SERVER['SERVER_NAME'],
	    $_SERVER['REQUEST_URI']
	  );
}
function pr( $array ){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

function get_cookie($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	// get headers too with this line
	curl_setopt($ch, CURLOPT_HEADER, 1);
	$result = curl_exec($ch);
	// get cookie
	// multi-cookie variant contributed by @Combuster in comments
	preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
	$cookies = array();
	foreach($matches[1] as $item) {
	    parse_str($item, $cookie);
	    $cookies = array_merge($cookies, $cookie);
	}
	return $cookies;
}


function get_next_id( $current_id ){
	global $db;
	$conditions = array(
		'id > '.$current_id,
		'updated = 0',
	);
	$next_id = $db->select('posts','id',$conditions,' id ASC ',1)->fetch();
	return $next_id->id;
}


