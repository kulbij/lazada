<?php
session_start();
include_once dirname(__FILE__).'/include.php';
include_once dirname(__FILE__).'/simple_html_dom.php';


function tool_get_product_discount($request){
	global $configs,$db;	
	$seller_id 		= ( isset($request['seller_id']) ) ? $request['seller_id'] : 0;
	$product_page 	= ( isset($request['product_page']) ) ? $request['product_page'] : 1;
	
	if( $product_page == 1 ){
		$_SESSION[$seller_id.'_total_product'] 			= 0;
		$_SESSION[$seller_id.'_total_product_discount'] = 0;
	}

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();
	if( !$seller_info ){
		$errors[] = 'Missing Seller Info';
	}

	if( count($errors) > 0 ){
		$request['msg']    = 'Can not get product , pls check !';
		$request['status'] = 0;
		$request['errors'] = $errors;
		echo json_encode($request);
		die();
	}
	$seller_product_url = $seller_info->seller_url;
	$seller_url = $seller_info->seller_url;
	$seller_url = str_replace('//?dir','/?dir', $seller_url);

	if( $_SESSION[$seller_id.'_total_product'] == 0 ){
		$html = file_get_html( $seller_url );
		$all_pageObjs = array();
		$max_page_all = 0;
		foreach ($html->find('.c-paging__link') as $link) {
			$all_pageObjs[] = $link->plaintext;
		}
		$max_page_all = end($all_pageObjs);
		$max_page_all = ( $max_page_all ) ? $max_page_all : 1;
		$seller_url = $seller_info->seller_url.'/?itemperpage=30&page='.$max_page_all;
		$html = file_get_html( $seller_url );
		$product_objs 				= $html->find('.c-product-card__price-final');
		if( count( $product_objs ) > 0 ){
			$total_product 		= count($product_objs);
			$total_product_all 	= ( 36 *  (int)$max_page_all );
			$total_product_all = ( (int)$total_product_all - 36 ) + count( $product_objs );
			$_SESSION[$seller_id.'_total_product'] 	= $total_product_all;
		}
	}


	
	$seller_url = $seller_info->seller_url.'/?itemperpage=30&sort=discountspecial&page='.$product_page;
	$html = file_get_html( $seller_url );
	$product_discount_objs 		= $html->find('.c-product-card__old-price');
	if( count( $product_discount_objs ) > 0 ){
		$total_product_discount 	= $_SESSION[$seller_id.'_total_product_discount'];
		$total_product_discount 	+= count($product_discount_objs);
		$_SESSION[$seller_id.'_total_product_discount'] 	= $total_product_discount;


		$request['product_page'] = (int)$product_page + 1;
		$request['msg']    		= 'Start count discount product at page '.$request['product_page'];
		$request['status'] 		= 1;
		$request['seller_id'] 	= $seller_id;
		$request['action'] 		= 'tool_get_product_discount';
		
	}else{

		$total_product 			= $_SESSION[$seller_id.'_total_product'];
		$total_product_discount = $_SESSION[$seller_id.'_total_product_discount'];
		$dataInsert = array(
			'total_product' 		 => $total_product,
			'total_product_discount' => $total_product_discount,
			'total_product_non_discount' => ( $total_product - $total_product_discount )
		);
		$db->update( 'sellers', $dataInsert,$seller_id, 'seller_id');

		$request['msg']    		= 'All done ! Showing seller info again.';
		$request['status'] 		= 1;
		$request['action'] 		= 'tool_reshow_info';
		$request['seller_id'] 	= $seller_id;
	}

	echo json_encode($request);
	die();

}
function tool_get_product_reviews($request){

	global $configs,$db;	
	$seller_id 		= ( isset($request['seller_id']) ) ? $request['seller_id'] : 0;
	$review_page 	= ( isset($request['review_page']) ) ? $request['review_page'] : 1;
	

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();
	if( !$seller_info ){
		$errors[] = 'Missing Seller Info';
	}

	if( count($errors) > 0 ){
		$request['msg']    = 'Can not get product reivew, pls check !';
		$request['status'] = 0;
		$request['errors'] = $errors;
		echo json_encode($request);
		die();
	}
	$seller_product_review_url = 'https://www.lazada.sg'.$seller_info->product_reviews_link;
	$seller_product_review_url = str_replace('/products','/product-reviews',$seller_product_review_url);
	$seller_product_review_url = $seller_product_review_url.'/?page='.$review_page.'&size=10';
	$product_reviews 	= file_get_contents( $seller_product_review_url );
	$product_reviews 	= json_decode($product_reviews);
	
	if( count( $product_reviews->data ) > 0 ){
		$html = new simple_html_dom();
		$product_reviews_data = array();
		foreach ($product_reviews->data as $product_review_html) {
			$html->load($product_review_html);
			$reivew_ratings = array();
			foreach ( $html->find('.c-review__title') as $item_val ) {
				$review__title = $item_val->plaintext;
				if( $review__title ) continue;
			}
			foreach ( $html->find('.c-review__date') as $item_val ) {
				$review__date = $item_val->plaintext;
				if( $review__date ) continue;
			}
			foreach ( $html->find('.c-review__name-text') as $item_val ) {
				$review__name = $item_val->plaintext;
				if( $review__name ) continue;
			}
			foreach ( $html->find('.c-review__comment') as $item_val ) {
				$review__comment = $item_val->plaintext;
				if( $review__comment ) continue;
			}
			foreach ( $html->find('.c-review__product-title') as $item_val ) {
				$product__title = $item_val->plaintext;
				if( $product__title ) continue;
			}
			foreach ( $html->find('.c-review__image') as $item_val ) {
				$product__img = $item_val->getAttribute('src');
				if( $product__img ) continue;
			}
			foreach ( $html->find('.c-review__product-thumbnail a') as $item_val ) {
				$product__link = $item_val->getAttribute('href');
				if( $product__link ) continue;
			}

			foreach ( $html->find('.c-rating-stars__active') as $item_val ) {
				$reivew_rating = $item_val->getAttribute('style');
				if( $reivew_rating ) continue;
			}

			if( $reivew_rating ){
				preg_match('!\d+!', $reivew_rating, $matches);
				$reivew_rating_percent = $matches[0]; 
				$reivew_rating = ceil($reivew_rating_percent/20);
				
			}

			$product_reviews_data[] = array(
				'title' 		=> @$review__title,
				'seller_id' 	=> $seller_id,
				'name' 			=> @$review__name,
				'text' 			=> @$review__comment,
				'rating' 		=> $reivew_rating,
				'pro_image_url' => @$product__img,
				'pro_title' 	=> @$product__title,
				'pro_url' 		=> @$product__link,
			);
		}

		if( $review_page == 1 ){
			$query = "DELETE FROM product_reviews WHERE seller_id = ".$seller_id;
			$db->raw($query);
		}
		if( count( $product_reviews_data ) > 0 ){
			foreach ($product_reviews_data as $product_review) {
				$dataInsert = $product_review;
				$db->create('product_reviews',$dataInsert);
			}
		}

		$request['msg']    		= 'Got product reviews at page '.$review_page;
		$request['status'] 		= 1;
		$request['review_page'] = ( (int)$review_page + 1 );
		$request['seller_id'] 	= $seller_id;
		$request['action'] 		= 'tool_get_product_reviews';
	}else{
		$request['msg']    = 'Start count discount product at page 1';
		$request['status'] 		= 1;
		$request['seller_id'] 	= $seller_id;
		$request['action'] 		= 'tool_get_product_discount';

		$dataInsert = array(
			'total_product' 		 => 0,
			'total_product_discount' => 0,
		);
		$update = $db->update( 'sellers', $dataInsert,$seller_id, 'seller_id');
	}

	echo json_encode($request);
	die();
}

function tool_get_reviews($request){
	global $configs,$db;	
	$seller_id 		= ( isset($request['seller_id']) ) ? $request['seller_id'] : 0;
	$review_page 	= ( isset($request['review_page']) ) ? $request['review_page'] : 1;
	$review_type 	= ( isset($request['review_type']) ) ? $request['review_type'] : 0;
	$review_types = array(
		'positive','neutral','negative'
	);

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();
	if( !$seller_info ){
		$errors[] = 'Missing Seller Info';
	}

	if( count($errors) > 0 ){
		$request['msg']    = 'Can not get reivew, pls check !';
		$request['status'] = 0;
		$request['errors'] = $errors;
		echo json_encode($request);
		die();
	}
	

	if( @isset($review_types[$review_type]) ){

		$review_type_string = $review_types[$review_type];
		$total_page = 0;
		switch ($review_type_string) {
			case 'positive':
				$total_records  = $seller_info->seller_reviews_positive;
				break;
			case 'neutral':
				$total_records  = $seller_info->seller_reviews_neutral;
				break;
			case 'negative':
				$total_records  = $seller_info->seller_reviews_negative;
				break;
			default:
				$total_records  = $seller_info->seller_reviews_positive;
				break;
		}
		$total_page = ceil( $total_records  / 10 );
		$total_page = (int)$total_page;
		$review_type = (int)$review_type;

		


		$seller_review_url = 'https://www.lazada.sg'.$seller_info->seller_reviews_link;
		$seller_review_url = $seller_review_url.'/reviews/?type='.$review_type_string.'&p='.$review_page;

		$html 	= file_get_html( $seller_review_url );
		if( $html ){
			$seller_reviews = array();
			foreach($html->find('.seller-review-item') as $item) 
			{
			     
			     foreach ($item->find('.seller-review-item__author') as $item_val) {
			     	$name = $item_val->plaintext;
			     }
			     foreach ($item->find('.seller-review-item__content') as $item_val) {
			     	$text = $item_val->plaintext;
			     }
			     foreach ($item->find('.seller-review-item__time') as $item_val) {
			     	$created_at_string = $item_val->plaintext;
			     }
			    
			     $seller_reviews[] = array(
			     	'seller_id' 		=> $seller_id,
			     	'name' 				=> $name,
			     	'text' 				=> $text,
			     	'review_type' 		=> $review_type_string,
			     	'created_at_string' => $created_at_string,
			     );
			}

			if( $review_page == 1 ){
				$query = "DELETE FROM seller_reviews WHERE seller_id = ".$seller_id." AND review_type = '".$review_type_string."'";
				$db->raw($query);
			}
			


			if( count($seller_reviews) > 0){
				foreach ($seller_reviews as $seller_review) {
					$dataInsert = array();
					$dataInsert['seller_id'] = $seller_review['seller_id'];
					$dataInsert['name'] = $seller_review['name'];
					$dataInsert['text'] = $seller_review['text'];
					$dataInsert['created_at_string'] = $seller_review['created_at_string'];
					$dataInsert['review_type'] = $seller_review['review_type'];
					$db->create('seller_reviews',$dataInsert);
				}
			}
			
			$request['msg']    = 'Got seller reviews at page '.$review_page.' and review type '.$review_type_string;
			$request['status'] 		= 1;
			$request['seller_id'] 	= $seller_id;
		}
		
		

		if( (int)$review_page < $total_page ){
			$request['review_page'] = $review_page + 1;
			$request['review_type'] = $review_type;
		}
		if( (int)$review_page == $total_page ){
			$request['review_page'] = 1;
			$request['review_type'] = (int)$review_type + 1;	
		}
		if( $review_type == 2 || $total_page == 0){
			$request['review_page'] = 1;
			$request['review_type'] = 3;	
		}
		$request['action'] 		= 'tool_get_reviews';
	}else{
		$request['status'] 		= 1;
		$request['action'] 		= 'tool_get_product_reviews';
		$request['seller_id'] 	= $seller_id;
		$request['msg']    		= 'Start get product reviews';
	}
	echo json_encode($request);
	die();

}
function tool_reshow_info($request){
	global $configs,$db;	
	$seller_id 		= ( isset($request['seller_id']) ) ? $request['seller_id'] : 0;
	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();
	if( !$seller_info ){
		$errors[] = 'Missing Seller Info';
	}

	if( count($errors) > 0 ){
		$request['msg']    = 'Can not get data, pls check !';
		$request['status'] = 0;
		$request['errors'] = $errors;
		echo json_encode($request);
		die();
	}

	$request['status'] 	= 1;
	$request['data'] 	= $seller_info;
	$request['msg']    	= 'All Done !';
	echo json_encode($request);
	die();
}
function tool_show_info($request){
	global $configs,$db;	
	$seller_id 		= ( isset($request['seller_id']) ) ? $request['seller_id'] : 0;
	$seller_info 	= $db->read('sellers',$seller_id,'seller_id')->fetch();
	$seller_info->top_rated = ( $seller_info->top_rated ) ? 'Yes' : 'No';

	if( $seller_info ){
		$xhtml = '';
		$xhtml .= '
			<tr>
				<td class="t_lable">Seller Name</td>
				<td>'.$seller_info->name.'</td>
			</tr>';
		$xhtml .= '
			<tr>
				<td class="t_lable">Location</td>
				<td>'.$seller_info->location.'</td>
			</tr>';	
		$xhtml .= '		
			<tr>
				<td class="t_lable">Seller ID</td>
				<td>'.$seller_info->seller_id.'</td>
			</tr>';

		$xhtml .= '
			<tr>
				<td class="t_lable">Seller Rating</td>
				<td>'.$seller_info->rate.' / 5 </td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Top Seller Star</td>
				<td>'.$seller_info->top_rated.'</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Seller Brands</td>
				<td>'.ucfirst($seller_info->brands).'</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Seller Categories</td>
				<td>'.ucfirst($seller_info->categories).'</td>
			</tr>
		';
		
		$xhtml .= '
			<tr>
				<td class="t_lable">Review Numbers</td>
				<td>
					<table class="table">
					    <thead>
					      <tr>
					        <th>Positive</th>
					        <th>Neutral</th>
					        <th>Negative</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>'.$seller_info->seller_reviews_positive.'</td>
					        <td>'.$seller_info->seller_reviews_neutral.'</td>
					        <td>'.$seller_info->seller_reviews_negative.'</td>
					      </tr>
					    </tbody>
					  </table>
				</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Total Seller Rating</td>
				<td>'.( $seller_info->seller_reviews_positive + $seller_info->seller_reviews_neutral + $seller_info->seller_reviews_negative).' Seller Ratings and Reviews</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Seller Reviews</td>
				<td><div class="seller_reviews">Loading...</div></td>
			</tr>
		';

		$better_seller = ( $seller_info->shipped_on_time_seller_rate - $seller_info->shipped_on_time_average_rate );
		$better_seller = abs($better_seller);
		$xhtml .= '
			<tr>
				<td class="t_lable">Shipped On Time</td>
				<td>'.$seller_info->shipped_on_time_seller_rate.'% ( '.( $better_seller ).' % better than other sellers in same category )</td>
			</tr>
		';


		$xhtml .= '
			<tr>
				<td class="t_lable">Product Reviews</td>
				<td><div class="product_reviews">Loading...</div></td>
			</tr>
		';

		$xhtml .= '
			<tr>
				<td class="t_lable">Total Product Reviews</td>
				<td>'.$seller_info->product_reviews_total.'</td>
			</tr>
		';

		$xhtml .= '
			<tr>
				<td class="t_lable">Delivery & Payment</td>
				<td>'.$seller_info->deliveries.'</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Range of price</td>
				<td>'.$seller_info->price_min.' - '.$seller_info->price_max.'</td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Number Discount</td>
				<td><div class="number_discount">Loading...</div></td>
			</tr>
		';
		$xhtml .= '
			<tr>
				<td class="t_lable">Number Non Discount</td>
				<td><div class="number_non_discount">Loading...</div></td>
			</tr>
		';

		$xhtml .= '
			<tr>
				<td class="t_lable">Main Category</td>
				<td>'.$seller_info->category.'</td>
			</tr>
		';

		$xhtml .= '
			<tr>
				<td class="t_lable">Time On Lazada</td>
				<td>'.$seller_info->time_on_lazada.' months</td>
			</tr>
		';

		$xhtml .= '
			<tr>
				<td class="t_lable">Size</td>
				<td>'.$seller_info->size.'</td>
			</tr>
		';

		$request['status'] 		= 1;
		$request['msg']    		= 'Getting reviews for Seller';
		$request['seller_id'] 	= $seller_id;
		$request['xhtml'] 		= $xhtml;
		$request['action'] 		= 'tool_get_reviews';
	}else{
		$request['msg']    = 'Something wrong !';
		$request['status'] = 0;
	}
	echo json_encode($request);
	die();

	
}

function tool_start($request){
	global $configs,$db;	
	
	if( isset($request['store_url']) && $request['store_url'] != "" ){
		$html 	= file_get_html($request['store_url']);
		$seller_id = 0;
		if( $html ){
			foreach($html->find('body') as $item) 
			{
			     $seller_id = $item->getAttribute('data-spm');
			     if( $seller_id ) break;
			}
			$jsons = array();
			foreach ($html->find('script[type="application/ld+json"]') as $key => $value) {
				$jsons[] = $value->innertext;
			}
			$unit = '';
			if( count( $jsons ) > 0 ){
				foreach ($jsons as $json) {
					$json_data = json_decode($json);
					$unit = $json_data->itemListElement[0]->offers->priceCurrency;
					if( $unit ) break;
				}
			}
		}
		
		

		if( !$seller_id  ){
			$request['msg']    = 'Can not find seller data';
			$request['status'] = 0;
			echo json_encode($request);
			die();
		}
		$categories = array();
		foreach($html->find('.c-catalog-nav__list a') as $item) 
		{
		     $categories[] = ucfirst( $item->plaintext );
		}

		$brands = $deliveries = array();
		$price_min = $price_max = 0;
		foreach($html->find('.c-catalog-controller__filters .c-sidebar__section') as $key => $item) 
		{
		     if( $key == 1 ){
	     		foreach ($item->find('.c-form-control-checkbox__input') as $value_item) {
	     			 $brands[] = ucfirst( $value_item->getAttribute('value') );
	     		}
		     }
		     if( $key == 2 ){



		     	foreach ($item->find('.c-form-control-range-slider__input-min .c-form-control-input__input') as $value_item_price) {
		     		$price_min = $value_item_price->getAttribute('value');
		     	}
		     	foreach ($item->find('.c-form-control-range-slider__input-max .c-form-control-input__input') as $value_item_price) {
		     		$price_max = $value_item_price->getAttribute('value');
		     	}
		     }
		     if( $key == 3 ){
		     	
		     	// foreach ($item->find('.c-form-control-checkbox__input') as $value_item) {
	     		// 	 $deliveries[] = $value_item->getAttribute('value');
	     		// }
				foreach ($item->find('.c-form-control-checkbox ') as $value_items) {
					foreach ($value_items->find('.c-form-control-checkbox__custom-label') as $value_item) {
						$deliver =  $deliver_orginal = $value_item->plaintext;
						 preg_match('!\d+!', $deliver, $matches);
						 $deliver = $matches[0];
						 $deliver_orginal = str_replace($deliver,'', $deliver_orginal);
						 $deliver_orginal = str_replace('()','', $deliver_orginal);
					}
					$deliveries[] 	=  trim($deliver_orginal);
				}
		     }
		}

	

		
		if( $seller_id  ){
			$seller_id   = str_replace('seller-','', $seller_id);
			$seller_info = file_get_contents('http://seller-transparency-api.lazada.sg/v1/seller/transparency?platform=desktop&lang=en&seller_id='.$seller_id);
			$seller_info = json_decode($seller_info);

			if( $seller_info ){
				/*
				1)how good is his rating this example is 5/5 => DONE
				2)top seller star => DONE
				3) brands he carry (no brand count as 1)
				4) all categories he carry => DONE
				5) seller rating => SUM OF (6)
				6) all reviews numbers (9 positive, 1 neutral , 1 negative) => DONE
				7) all reviews text => SUB TABLES
				8) shipped on time %  => DONE
				8b)shipped on time vs other in same category => DONE
				9) product reviews and rating all text and review  => SUB TABLES
				10) how many product is at what rating or no rank => DONE
				11) i want to know delivery & payment => DONE
				12) the range of product he sell in price => DONE
				13) how many on discount , vs non discount
				14) seller main category => DONE
				15) time on lazada => DONE
				16) seller size => DONE
				*/
				$dataInsert = array(
					'seller_id' => $seller_id, 
					'seller_url' => $request['store_url'], 
					'price_min' => $unit.' '.$price_min, //(12)
					'price_max' => $unit.' '.$price_max, //(12)
					'name' 		=> $seller_info->seller->name,
					'rate' 		=> $seller_info->seller->rate, //(1),(2)
					'top_rated' => $seller_info->seller->top_rated,
					'categories'  => implode(', ', $categories), //(4)
					'deliveries'  => implode(', ', $deliveries), //(11)
					'category'  => $seller_info->seller->category->name, //(14)
					'brands'  	=> implode(', ', $brands), //(3)
					'size'  	=> $seller_info->seller->size, //(16)
					'location'  => $seller_info->seller->location, 
					'time_on_lazada'  				=> $seller_info->seller->time_on_lazada->months, //(15) 
					'shipped_on_time_seller_rate'  	=> $seller_info->seller->shipped_on_time->seller_rate, //(8)
					'shipped_on_time_average_rate' 	=> $seller_info->seller->shipped_on_time->average_rate, //(8b)
					'seller_reviews_positive' 		=> $seller_info->seller->seller_reviews->positive->total, //(6)
					'seller_reviews_neutral' 		=> $seller_info->seller->seller_reviews->neutral->total,
					'seller_reviews_negative' 		=> $seller_info->seller->seller_reviews->negative->total,
					'seller_reviews_link' 			=> $seller_info->seller->seller_reviews->link,
					'product_reviews_link' 			=> $seller_info->seller->product_reviews->reviews_page_link,
					'product_reviews_total' 		=> $seller_info->seller->product_reviews->total, //(10)
					'positive_seller_rating' 		=> @$seller_info->seller->positive_seller_rating, 
					'positive_seller_rating_grade' 	=> @$seller_info->seller->positive_seller_rating_grade
				);

				$check = $db->read('sellers',$seller_id ,'seller_id')->fetch();
				if( !$check ){
					$db->create('sellers',$dataInsert);
				}else{
					$db->update( 'sellers', $dataInsert,$seller_id, 'seller_id');
				}
			}

			$request['msg']    		= 'Get basic info done! Showing data now...';
			$request['status'] 		= 1;
			$request['seller_id'] 	= $seller_id;
			$request['action'] 		= 'tool_show_info';
			echo json_encode($request);
			die();
		}
	}
	$request['msg']    = 'Something wrong !';
	$request['status'] = 0;
	echo json_encode($request);
	die();
}

if( count( $_REQUEST ) > 0 ){
	$action = $_REQUEST['action'];
	switch ($action) {
		case 'tool_start':
			tool_start($_REQUEST);
			break;
		case 'tool_show_info':
			tool_show_info($_REQUEST);
			break;
		case 'tool_get_reviews':
			tool_get_reviews($_REQUEST);
			break;
		case 'tool_get_product_reviews':
			tool_get_product_reviews($_REQUEST);
			break;
		case 'tool_get_product_discount':
			tool_get_product_discount($_REQUEST);
			break;
		case 'tool_reshow_info':
			tool_reshow_info($_REQUEST);
			break;
		default:
			$request['msg']    = 'All DONE';
			$request['status'] = 0;
			echo json_encode($request);
			break;
	}
}
die();