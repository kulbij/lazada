<?php
	include dirname(__FILE__).'/include.php';
	global $configs,$db;	
	$seller_id 		= ( isset($_GET['id']) ) ? $_GET['id'] : 0;
	

	$errors = array();
	if( !$seller_id  ){
		$errors[] = 'Missing Seller ID';
	}

	
	$product_reviews  = array();
	if( count($errors) > 0 ){
		pr($errors);
	}else{
		$product_reviews 	= $db->read('product_reviews',$seller_id,'seller_id')->all();
		
	}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Scrap Tool</title>
  <link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
  <style type="text/css">
     .form-control {
     margin-left: 0px;
     }
     .page-header, .page-header small {
     color: white !important;
     }
     label {
     color: black;
     }
     div#div_result {
        height: 129px;
        overflow-x: hidden;
    }
    body{
      font-size: 15px;
    }
    .t_lable {
      width: 25%;
    }
  </style>
  <script src="assets/jquery.min.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
      <div class="col-lg-12">
         <h1 class="page-header" style="color: black !important;">
            Product Reviews
         </h1>
      </div>
   </div>
	<div class="row">
      <div class="col-lg-12">
      	<?php
      		if( count( $product_reviews ) > 0 ){
      			foreach ($product_reviews as $product_review) {
      				?>
      				<table class="table table-bordered">
			          <tbody>
			          		<tr>
								<td class="t_lable">Name</td>
								<td><?php echo $product_review->name; ?></td>
							</tr>
							<tr>
								<td class="t_lable">Rating</td>
								<td><?php echo $product_review->rating; ?></td>
							</tr>
			            	<tr>
								<td class="t_lable">Title</td>
								<td><?php echo $product_review->title; ?></td>
							</tr>
							<tr>
								<td class="t_lable">Text</td>
								<td><?php echo $product_review->text; ?></td>
							</tr>
							<tr>
								<td class="t_lable">Product Title</td>
								<td><?php echo $product_review->pro_title; ?></td>
							</tr>

							<tr>
								<td class="t_lable">Product Url</td>
								<td>
									<?php
										if( $product_review->pro_url ){
											echo '<a target="_blank" href="https://www.lazada.sg'.$product_review->pro_url.'">https://www.lazada.sg'.$product_review->pro_url.'</a>';
										}
									?>
								</td>
							</tr>
							<tr>
								<td class="t_lable">Product Image Url</td>
								<td>
									<?php
										if($product_review->pro_image_url ){
											echo '<img width="150" height="150" src="'.$product_review->pro_image_url.'">';
										}

									?>
								</td>
							</tr>
							
			          </tbody>
			        </table>
      				<?php
      			}
      		}

      	?>
        
      </div>
   </div>
</div>
</body>
</html>