<?php

$router->get('/', [
    'as' => 'index', 'uses' => 'LazadaApiController@index'
]);

$router->post('/ajax/lazadaapi', [
    'as' => 'ajaxGetDataLazadaApi', 'uses' => 'LazadaApiController@ajaxGetData'
]);

$router->post('/ajax/qoo10front', [
    'as' => 'ajaxGetDataQoo10Front', 'uses' => 'Qoo10FrontController@ajaxGetData'
]);

$router->group(['prefix' => 'ajax/lazadafront'], function () use ($router) {
	$router->post('getdata', [
	    'as' => 'lazadaFrontGetData',
	    'uses' => 'LazadaFrontController@ajaxGetData'
	]);

	$router->post('get/seller/reviews', [
	    'as' => 'lazadaFrontGetSellerReviews',
	    'uses' => 'LazadaFrontController@ajaxGetSellerReviews'
	]);

	$router->post('get/product/reviews', [
	    'as' => 'lazadaFrontGetProductReviews',
	    'uses' => 'LazadaFrontController@ajaxGetProductReviews'
	]);

	$router->post('get/product/discount', [
	    'as' => 'lazadaFrontGetProductDiscount',
	    'uses' => 'LazadaFrontController@ajaxGetProductDiscount'
	]);
});

$router->get('/seller-reviews/{sellerId}', [
    'as' => 'sellerReviews',
    'uses' => 'LazadaFrontController@showSellerReviews'
]);

$router->get('/product-reviews/{sellerId}', [
    'as' => 'productReviews',
    'uses' => 'LazadaFrontController@showProductReviews'
]);
