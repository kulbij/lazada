<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <style>
      .loader {
          border: 16px solid #f3f3f3;
          border-top: 8px solid #3498db;
          border-radius: 50%;
          width: 50px;
          height: 50px;
          animation: spin 2s linear infinite;
      }

      .loadermini {
          border: 4px solid #f3f3f3;
          border-top: 3px solid #3498db;
          border-radius: 50%;
          width: 25px;
          height: 25px;
          animation: spin 1s linear infinite;
      }

      @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
      }

      .t_lable {
          width: 25%;
      }
    </style>
    
</head>

<body>
<div class="container">
    <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header" style="color: black !important;">
            Seller Reviews
         </h1>
      </div>
   </div>
    <div class="row">
      <div class="col-lg-12">
      @foreach ($seller_reviews as $seller_review)
        <table class="table table-bordered">
          <tbody>
                <tr>
                    <td class="t_lable">Name</td>
                    <td>{{ $seller_review->name }}</td>
                </tr>
                <tr>
                    <td class="t_lable">Text</td>
                    <td>{{ $seller_review->text }}</td>
                </tr>
                <tr>
                    <td class="t_lable">Created</td>
                    <td>{{ $seller_review->created_at_string }}</td>
                </tr>

                <tr>
                    <td class="t_lable">Review Type</td>
                    <td>{{ $seller_review->review_type }}</td>
                </tr>
          </tbody>
        </table>
      @endforeach

      </div>
   </div>
</div>
</body>
</html>