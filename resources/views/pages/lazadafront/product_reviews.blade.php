<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <style>
      .loader {
          border: 16px solid #f3f3f3;
          border-top: 8px solid #3498db;
          border-radius: 50%;
          width: 50px;
          height: 50px;
          animation: spin 2s linear infinite;
      }

      .loadermini {
          border: 4px solid #f3f3f3;
          border-top: 3px solid #3498db;
          border-radius: 50%;
          width: 25px;
          height: 25px;
          animation: spin 1s linear infinite;
      }

      @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
      }

      .t_lable {
          width: 25%;
      }
    </style>
    
</head>

<body>
<div class="container">
    <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header" style="color: black !important;">
            Product Reviews
         </h1>
      </div>
   </div>
    <div class="row">
      <div class="col-lg-12">
      @foreach ($product_reviews as $product_review)
        <table class="table table-bordered">
            <tbody>
                <tr>
                  <td class="t_lable">Name</td>
                  <td>{{ $product_review->name }}</td>
                </tr>
                <tr>
                  <td class="t_lable">Rating</td>
                  <td>{{ $product_review->rating }}</td>
                </tr>
                      <tr>
                  <td class="t_lable">Title</td>
                  <td>{{ $product_review->title }}</td>
                </tr>
                <tr>
                  <td class="t_lable">Text</td>
                  <td>{{ $product_review->text }}</td>
                </tr>
                <tr>
                  <td class="t_lable">Product Title</td>
                  <td>{{ $product_review->pro_title }}</td>
                </tr>

                <tr>
                  <td class="t_lable">Product Url</td>
                  <td>
                    @if ($product_review->pro_url)
                        echo '<a target="_blank" href="https://www.lazada.sg{{$product_review->pro_url }}'">https://www.lazada.sg{{ $product_review->pro_url }}</a>';
                    @endif
                  </td>
                </tr>
                <tr>
                  <td class="t_lable">Product Image Url</td>
                  <td>
                    @if ($product_review->pro_image_url)
                         <img width="150" height="150" src="{{ $product_review->pro_image_url }}">';
                    @endif
                  </td>
                </tr>
          
            </tbody>
          </table>
      @endforeach

      </div>
   </div>
</div>
</body>
</html>