<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <style>
      .loader {
          border: 16px solid #f3f3f3;
          border-top: 8px solid #3498db;
          border-radius: 50%;
          width: 50px;
          height: 50px;
          animation: spin 2s linear infinite;
      }

      .loadermini {
          border: 4px solid #f3f3f3;
          border-top: 3px solid #3498db;
          border-radius: 50%;
          width: 25px;
          height: 25px;
          animation: spin 1s linear infinite;
      }

      @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
      }

      .t_lable {
          width: 25%;
      }
    </style>
    
</head>

<body>

<div class="container" style="padding-top: 3%;">

    <div class="jumbotron">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#lazadaback" aria-controls="lazadaback" role="tab"
                                                      data-toggle="tab">Lazada API Stats</a></li>
            <li role="presentation"><a href="#lazada_front_stats" aria-controls="lazada_front_stats" role="tab" data-toggle="tab">Lazada Front Stats</a>
            </li>
            <li role="presentation"><a href="#qoo10api" aria-controls="qoo10api" role="tab"
                                       data-toggle="tab">Qoo10 API Stats</a></li>
            <li role="presentation"><a href="#qoo10front" aria-controls="qoo10front" role="tab"
                                       data-toggle="tab">Qoo10 Front Stats</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="lazadaback">
                @include('partials.lazadaback_partial')
            </div>
            <div role="tabpanel" class="tab-pane" id="lazada_front_stats">
              @include('partials.lazadafront_partial')
            </div>
            <div role="tabpanel" class="tab-pane" id="qoo10api">3</div>
            <div role="tabpanel" class="tab-pane" id="qoo10front">
                @include('partials.qoo10front_partial')
            </div>
        </div>
    </div>

</div> <!-- /container -->


</body>
</html>
