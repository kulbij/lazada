<div class="clearfix" style="padding-top: 2%">
    <form method="post" action="{{ route('index') }}" id="qoo10front_form" role="form">
        <div class="form-row align-items-center">
            <input type="hidden" name="action" value="tool_start">
            <div class="form-group col-sm-10">
                <input type="text" name="store_url" class="form-control" placeholder="Store Url">
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-default">Get data</button>
            </div>
        </div>
    </form>

    <div id="ajax_put_block_qoo10">
        <div class="col-sm-12" id="run_qoo10"></div>
    </div>

</div>

<script>

    $('#qoo10front_form').on('submit', function(e){
        e.preventDefault();
        var store_url = $('form input[name="store_url"]').val();

        if (store_url == "") {
            var clearInput = "<p>Please specify Store URL address</p>";
            $('#ajax_put_block_qoo10 #run_qoo10').html(clearInput);
        }

        if (!isUrlValid(store_url)) {
            var clearInput = "<p>Not valid Store URL address</p>";
            $('#ajax_put_block_qoo10 #run_qoo10').html(clearInput);
            return;
        }

        $.ajax({
            type: "POST",
            url: '{{ route('ajaxGetDataQoo10Front') }}',
            data: {url: store_url},
            beforeSend: function(){
                $('#ajax_put_block_qoo10 #run_qoo10').html('<div class="loader"></div>');
                $('form input').attr('disabled', 'disabled');
                $('form button').attr('disabled', 'disabled');
            }
        }).done(function(data) {
            $('#ajax_put_block_qoo10 #run_qoo10').html(data);
            $('form input').removeAttr('disabled');
            $('form button').removeAttr('disabled');

            $.ajax({
                type: "POST",
                url: '{{ route('ajaxGetDataQoo10Front') }}',
                data: {url: store_url, field: 'likes'},
            }).done(function(data) {
                if (typeof data.field == 'object') {
                    $('#likes').parent().parent().hide();
                }
                $('#likes').replaceWith(data.field);

                $.ajax({
                    type: "POST",
                    url: '{{ route('ajaxGetDataQoo10Front') }}',
                    data: {url: store_url, field: 'solds'},
                }).done(function(data) {
                    if (typeof data.field == 'object') {
                        $('#solds').parent().parent().hide();
                    }
                    $('#solds').replaceWith(data.field);
                });
            });

            $.ajax({
                type: "POST",
                url: '{{ route('ajaxGetDataQoo10Front') }}',
                data: {url: store_url, field: 'reviews'},
            }).done(function(data) {
                var reviews = $('#reviews'),
                firstTotal = reviews.find('input').val();

                reviews.replaceWith(Number(firstTotal) + Number(data.field));

                if (typeof data.field == 'object') {
                    $('#reviews').parent().parent().hide();
                }
            });
        });
    });

    function isUrlValid(userInput) {
        var regexQuery = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
        var url = new RegExp(regexQuery,"i");
        if (url.test(userInput)) {
            return true;
        }
        return false;
    }

</script>