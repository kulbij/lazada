<div class="clearfix" style="padding-top: 2%">
    <form class="form-inline" method="post" id="lazadaapi_form" action="{{ route('index') }}">
        <div class="form-group">
            <input type="text" class="form-control" name="email" id="lazadaapi_email" placeholder="API Email">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="key" id="lazadaapi_key" placeholder="Secret key">
        </div>
        <button type="submit" class="btn btn-default">Get data</button>
    </form>
    <div id="lazadaApiContainer"></div>
    <div id="loadingSpinner" style="display: none">Loading...</div>
</div>

<script>

    $('#lazadaapi_form').on('submit', function(e){
        e.preventDefault();
        $('#loadingSpinner').show();

        var email = $('#lazadaapi_email').val();
        var key = $('#lazadaapi_key').val();

        $.ajax({
            type: "POST",
            url: '{{ route('ajaxGetDataLazadaApi') }}',
            data: {
                email: email,
                key: key,
            },
            success: function(data) {

                $('#loadingSpinner').hide();

                if(data && !data.error) {
                    $(data).appendTo('#lazadaApiContainer');
                }
                else {
                    alert('Error');
                }

            }
        });
    });

</script>