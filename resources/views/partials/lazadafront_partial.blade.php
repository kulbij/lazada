<div class="clearfix" style="padding-top: 2%">
    <form method="post" action="{{ route('index') }}" id="lazadafront_form" role="form">
        <div class="form-row align-items-center">
            <input type="hidden" name="action" value="tool_start">
            <div class="form-group col-sm-10">
                <input type="text" name="store_url_lazada" class="form-control" placeholder="Store Url">
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-default">Get data</button>
            </div>
        </div>
    </form>

    <div id="ajax_put_block_lazada">
        <div class="col-sm-12" id="run_lazada"></div>
    </div>
</div>

<script>

    $('#lazadafront_form').on('submit', function(e){
        e.preventDefault();
        var inputUrl = $('form#lazadafront_form input[name="store_url_lazada"]');
        var inputBtn = $('form#lazadafront_form button');
        var store_url = inputUrl.val();

        if (store_url == "") {
            var clearInput = "<p>Please specify Store URL address</p>";
            $('#ajax_put_block_lazada #run_lazada').html(clearInput);
        }

        if (!isUrlValid(store_url)) {
            var clearInput = "<p>Not valid Store URL address</p>";
            $('#ajax_put_block_lazada #run_lazada').html(clearInput);
            return;
        }

        $.ajax({
            type: "POST",
            url: '{{ route('lazadaFrontGetData') }}',
            data: {url: store_url},
            beforeSend: function(){
                $('#ajax_put_block_lazada #run_lazada').html('<div class="loader"></div>');
                inputUrl.attr('disabled', 'disabled');
                inputBtn.attr('disabled', 'disabled');
            }
        }).done(function(data) {

            if (data.status == 200) {
                $('#ajax_put_block_lazada #run_lazada').html(data.view);

                if (data.action == 'tool_get_reviews') {
                    tool_get_reviews(data.seller.seller_id);
                }
            }

            inputUrl.removeAttr('disabled');
            $('form button').removeAttr('disabled');
        });

    });

    function tool_get_reviews( seller_id,action,review_page,review_type )
    {
        $.ajax({
            type: "POST",
            url: '{{ route('lazadaFrontGetSellerReviews') }}',
            data: {'seller_id' : seller_id, 'action' : action, 'review_page' : review_page, 'review_type' : review_type}
        }).done(function(respon) {
            if( respon.status == 200 ){
                if( respon.action == 'tool_get_reviews' ){
                   tool_get_reviews(respon.seller_id,respon.action,respon.review_page,respon.review_type);
                }
                
                if ( respon.action == 'tool_get_product_reviews' ) {

                   $('#seller_reviews').replaceWith('<a target="_blank" href="{{ url() }}/seller-reviews/' + respon.seller_id + '">View Seller Reviews</a>');

                   tool_get_product_reviews(respon.seller_id,respon.action,1);
                }
            }
        });
     }

     function tool_get_product_reviews( seller_id,action,review_page )
     {
        var dta = {
            'action'      : 'tool_get_product_reviews',
            'seller_id'   : seller_id,
            'review_page'   : review_page,
        };

        $.ajax({
            type: "POST",
            url: '{{ route('lazadaFrontGetProductReviews') }}',
            data: dta
        }).done(function(respon) {
            if (respon.status == 200) {

                if (respon.action == 'tool_get_product_reviews') {
                   tool_get_product_reviews(respon.seller_id,respon.action,respon.review_page);
                }

                if (respon.action == 'tool_get_product_discount') {

                    $('#product_reviews').replaceWith('<a target="_blank" href="{{ url() }}/product-reviews/' + respon.seller_id + '">View Seller Reviews</a>');

                   tool_get_product_discount(respon.seller_id,respon.action,1);
                }
               
            }
        });
     }

    function tool_get_product_discount( seller_id,action,product_page ){
         var dta = {
            'action'      : 'tool_get_product_discount',
            'seller_id'   : seller_id,
            'product_page'   : product_page,
        };

        $.ajax({
            type: "POST",
            url: '{{ route('lazadaFrontGetProductDiscount') }}',
            data: dta
        }).done(function(respon) {
            if(respon.status == 200){
                if(respon.action == 'tool_get_product_discount'){
                   tool_get_product_discount(respon.seller_id,respon.action,respon.product_page);
                }

                if (respon.action == 'tool_reshow_info') {
                    $('#number_discount').replaceWith(respon.product_discount);
                    $('#number_non_discount').replaceWith(respon.product_non_discount);

                //    tool_reshow_info(respon.seller_id,respon.action);
                }
               
            }
        });
     }

    function isUrlValid(userInput) {
        var regexQuery = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
        var url = new RegExp(regexQuery,"i");
        if (url.test(userInput)) {
            return true;
        }
        return false;
    }

</script>