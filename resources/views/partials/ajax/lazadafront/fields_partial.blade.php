<table class="table table-bordered" id="seller-info">
    <tbody>
        @if ($seller->name)
            <tr>
                <td class="t_lable">Seller Name</td>
                <td>{{ $seller->name }}</td>
            </tr>
        @endif
        @if ($seller->location)
            <tr>
                <td class="t_lable">Location</td>
                <td>{{ $seller->location }}</td>
            </tr>
        @endif
        @if ($seller->seller_id)
            <tr>
                <td class="t_lable">Seller ID</td>
                <td>{{ $seller->seller_id }}</td>
            </tr>
        @endif
        @if ($seller->rate)
            <tr>
                <td class="t_lable">Seller Rating</td>
                <td>{{ $seller->rate }} / 5 </td>
            </tr>
        @endif
        <tr>
            <td class="t_lable">Top Seller Star</td>
            <td>{{ $seller->top_rated ? 'Yes' : 'No' }}</td>
        </tr>
        @if (ucfirst($seller->brands))
            <tr>
                <td class="t_lable">Seller Brands</td>
                <td>{{ ucfirst($seller->brands) }}</td>
            </tr>
        @endif
        @if (ucfirst($seller->categories))
            <tr>
                <td class="t_lable">Seller Categories</td>
                <td>{{ ucfirst($seller->categories) }}</td>
            </tr>
        @endif
        <tr>
            <td class="t_lable">Review Numbers</td>
            <td>
            <table class="table">
                <thead>
                    <tr>
                        <th>Positive</th>
                        <th>Neutral</th>
                        <th>Negative</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $seller->seller_reviews_positive }}</td>
                        <td>{{ $seller->seller_reviews_neutral }}</td>
                        <td>{{ $seller->seller_reviews_negative }}</td>
                    </tr>
                </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td class="t_lable">Total Seller Rating</td>
            <td>{{ ( $seller->seller_reviews_positive + $seller->seller_reviews_neutral + $seller->seller_reviews_negative) }} Seller Ratings and Reviews</td>
        </tr>
        <tr>
            <td class="t_lable">Seller Reviews</td>
            <td><div class="seller_reviews">
                <div id="seller_reviews" class="loadermini">
                    <input type="hidden" name="seller_reviews" value="seller_reviews">
                </div>
            </div></td>
        </tr>
        <tr>
            <td class="t_lable">Shipped On Time</td>
            <td>{{ $seller->shipped_on_time_seller_rate }}% ( {{ abs( $seller->shipped_on_time_seller_rate - $seller->shipped_on_time_average_rate ) }} % better than other sellers in same category )</td>
        </tr>
        <tr>
            <td class="t_lable">Product Reviews</td>
            <td><div class="product_reviews">
                <div id="product_reviews" class="loadermini">
                    <input type="hidden" name="product_reviews" value="product_reviews">
                </div>
            </div></td>
        </tr>
        <tr>
            <td class="t_lable">Total Product Reviews</td>
            <td>{{ $seller->product_reviews_total }}</td>
        </tr>
        @if ($seller->deliveries)
            <tr>
                <td class="t_lable">Delivery & Payment</td>
                <td>{{ $seller->deliveries }}</td>
            </tr>
        @endif
        <tr>
            <td class="t_lable">Range of price</td>
            <td>{{ $seller->price_min }} - {{ $seller->price_max }}</td>
        </tr>
        <tr>
            <td class="t_lable">Number Discount</td>
            <td><div class="number_discount">
                <div id="number_discount" class="loadermini">
                    <input type="hidden" name="number_discount" value="number_discount">
                </div>
            </div></td>
        </tr>
        <tr>
            <td class="t_lable">Number Non Discount</td>
            <td><div class="number_non_discount">
                <div id="number_non_discount" class="loadermini">
                    <input type="hidden" name="number_non_discount" value="number_non_discount">
                </div>
            </div></td>
        </tr>
        @if ($seller->category)
            <tr>
                <td class="t_lable">Main Category</td>
                <td>{{ $seller->category }}</td>
            </tr>
        @endif
        <tr>
            <td class="t_lable">Time On Lazada</td>
            <td>{{ $seller->time_on_lazada }} months</td>
        </tr>
        <tr>
            <td class="t_lable">Size</td>
            <td>{{ $seller->size }}</td>
        </tr>
    </tbody>
</table>