<table class="table table-bordered" id="seller-info">
    <tbody>

    @if (sizeof($data) > 1)
        @foreach ($data as $key => $item)

            @if (($item['type'] == 'text'))
                <tr>
                    <td class="{{ $item['class'] }}">{{ $item['label'] }}</td>
                    <td>
                        @if (isset($item['preloader']) && $item['preloader'] === true)
                            <div
                            @if (isset($item['field']))
                             id="{{ $item['field'] }}"
                             @endif
                             class="loadermini"><input type="hidden" name="{{ $item['field'] }}" value="{{ $item['value'] }}"></div>
                        @else
                            {{ $item['value'] }}
                        @endif
                    </td>
                </tr>
            @elseif ($item['type'] == 'table')
                <tr>
                    <td class="{{ $item['class'] }}">{{ $item['label'] }}</td>
                    <td>
                    @if (sizeof($item['values']) > 0)
                        <table class="table">
                            <thead>
                              <tr>
                              @foreach ($item['values'] as $value)
                                    <th>{{ $value['label'] }}</th>
                                @endforeach
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                              @foreach ($item['values'] as $value)
                                    <td>{{ $value['value'] }}</td>
                                @endforeach
                              </tr>
                            </tbody>
                        </table>
                    @endif
                    </td>
                </tr>
            @elseif ($item['type'] == 'link')
                <tr>
                    <td class="{{ $item['class'] }}">{{ $item['label'] }}</td>
                    <td><div class="seller_reviews">
                    <a target="_blank" href="{{ $item['href'] }}">{{ $item['value'] }}</a></div></td>
                </tr>
            @endif

        @endforeach
        
    @endif

    </tbody>
</table>