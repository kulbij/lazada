<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLazadaData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id')->nullable();
            $table->string('seller_url')->nullable();
            $table->string('name')->nullable();
            $table->string('rate')->nullable();
            $table->integer('top_rated')->nullable();
            $table->text('category')->nullable();
            $table->integer('size')->nullable();
            $table->string('location')->nullable();
            $table->string('time_on_lazada')->nullable();
            $table->string('shipped_on_time_seller_rate')->nullable();
            $table->string('shipped_on_time_average_rate')->nullable();
            $table->string('seller_reviews_link')->nullable();
            $table->string('product_reviews_link')->nullable();
            $table->text('brands')->nullable();
            $table->string('categories')->nullable();
            $table->integer('seller_reviews_positive')->nullable();
            $table->integer('seller_reviews_neutral')->nullable();
            $table->integer('seller_reviews_negative')->nullable();
            $table->string('deliveries')->nullable();
            $table->string('price_min')->nullable();
            $table->string('price_max')->nullable();
            $table->string('product_reviews_total')->nullable();
            $table->integer('total_product')->nullable();
            $table->integer('total_product_discount')->default(false);
            $table->integer('total_product_non_discount')->default(false);
            $table->string('positive_seller_rating')->nullable();
            $table->string('positive_seller_rating_grade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sellers');
    }
}
