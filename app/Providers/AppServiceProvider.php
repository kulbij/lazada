<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Http\LazadaApi\LazadaAPI;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	require_once base_path('app/Helpers/simple_html_dom.php');
    }
}
