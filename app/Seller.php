<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sellers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id',
        'seller_url',
        'name',
        'rate',
        'top_rated',
        'category',
        'size',
        'location',
        'shipped_on_time_seller_rate',
        'shipped_on_time_average_rate',
        'seller_reviews_link',
        'product_reviews_link',
        'brands',
        'categories',
        'seller_reviews_positive',
        'seller_reviews_neutral',
        'seller_reviews_negative',
        'deliveries',
        'price_min',
        'price_max',
        'product_reviews_total',
        'total_product',
        'total_product_discount',
        'total_product_non_discount',
        'positive_seller_rating',
        'positive_seller_rating_grade',
        'time_on_lazada'
    ];

    public function user()
    {
        return $this->all();
    }
}
