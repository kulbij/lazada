<?php

namespace App\Traits;

use App\Seller;
use simple_html_dom;
use App\SellerReview;
use App\ProductReview;

trait LazadaFrontTrait {

	public function processSeller(array $dataInsert, int $sellerId)
    {
        if ($seller = $this->getSeller($sellerId)) {
            return $this->updateSeller($seller, $dataInsert);
        }

        return $this->createSeller($dataInsert);
    }

    public function getSeller(int $sellerId)
    {
        return Seller::where('seller_id', $sellerId)->first();
    }

    public function createSeller($dataInsert)
    {
        return Seller::create($dataInsert);
    }

    public function updateSeller(Seller $seller, array $dataInsert)
    {
        $seller->update($dataInsert);

        return $seller;
    }

    public function getSellerId($html)
    {
        foreach ($html->find('body') as $item) {
             if ($sellerId = $item->getAttribute('data-spm')) {
                return $sellerId;
             }
        }

        return false;
    }

    public function getJsons($html)
    {
        $jsons = [];

        foreach ($html->find('script[type="application/ld+json"]') as $key => $value) {
            $jsons[] = $value->innertext ?? null;
        }

        return array_filter($jsons);
    }

    public function getUnit(array $jsons)
    {
        if (sizeof($jsons) <= 0) {
            return null;
        }

        $unit = '';

        foreach ($jsons as $json) {
            $jsonData = json_decode($json);
            if ($unit = $this->testValue($jsonData->itemListElement[0]->offers->priceCurrency)) {
                return $unit;
            }
        }

        return $unit;
    }

    public function testValue($property)
    {
        return $property ?? false;
    }

    public function getCategories($html)
    {
        $categories = [];

        foreach($html->find('.c-catalog-nav__list a') as $item) {
            $categories[] = ucfirst($item->plaintext) ?? null;
        }

        return array_filter($categories);
    }

    public function getBrands($html)
    {
        $brands = [];

        $item = $this->getCatalogItem($html, 1);
        
        if (!$item) {
            return false;
        }

        foreach ($item->find('.c-form-control-checkbox__input') as $value_item) {
            $brands[] = ucfirst($value_item->getAttribute('value'));
        }

        return $brands;
    }

    public function getPriceMax($html)
    {
        $item = $this->getCatalogItem($html, 2);

        if (!$item) {
            return false;
        }

        $priceMax = 0;

        foreach ($item->find('.c-form-control-range-slider__input-max .c-form-control-input__input') as $value_item_price) {
            $priceMax = $value_item_price->getAttribute('value');
        }

        return $priceMax;
    }

    public function getPriceMin($html)
    {
        $item = $this->getCatalogItem($html, 2);

        if (!$item) {
            return false;
        }

        $priceMin = 0;

        foreach ($item->find('.c-form-control-range-slider__input-min .c-form-control-input__input') as $value_item_price) {
            $priceMin = $value_item_price->getAttribute('value');
        }

        return $priceMin;
    }

    public function getDeliveries($html)
    {
        $item = $this->getCatalogItem($html, 3);

        if (!$item) {
            return false;
        }

        $deliveries = [];

        foreach ($item->find('.c-form-control-checkbox ') as $value_items) {
            foreach ($value_items->find('.c-form-control-checkbox__custom-label') as $value_item) {
                $deliver =  $deliver_orginal = $value_item->plaintext;
                preg_match('!\d+!', $deliver, $matches);
                $deliver = $matches[0];
                $deliver_orginal = str_replace($deliver,'', $deliver_orginal);
                $deliver_orginal = str_replace('()','', $deliver_orginal);
            }
            
            $deliveries[]   =  trim($deliver_orginal);
        }

        return $deliveries;
    }

    public function getCatalogItem($html, int $key = 1)
    {
        return $html->find('.c-catalog-controller__filters .c-sidebar__section')[$key] ?? false;
    }

    public function processSellerReview(int $sellerId, array $fields)
    {
        if ($sellerReview = $this->getSellerReview($sellerId)) {
            return $this->updateSellerReview($sellerReview, $fields);
        }

        return $this->createSellerReview($fields);
    }

    public function getSellerReview(int $sellerId)
    {
    	return SellerReview::where('seller_id', $sellerId)->first();
    }

    public function getSellerReviews(int $sellerId)
    {
    	return SellerReview::where('seller_id', $sellerId)->get();
    }

    public function createSellerReview(array $fields)
    {
    	return SellerReview::create($fields);
    }

    public function createMenySellerReview(int $sellerId, array $sellerReviews)
    {
    	foreach ($sellerReviews as $sellerReview) {
    		$this->createSellerReview($sellerReview);
    	}

    	return true;
    }

    public function updateSellerReview(SellerReview $sellerReview, array $fields)
    {
    	$sellerReview->update($fields);

        return $sellerReview;
    }

    public function deleteSellerReview(int $sellerId)
    {
    	SellerReview::where('seller_id', $sellerId)->delete();
    }

    public function getProductSellers(int $sellerId)
    {	
    	return ProductReview::where('seller_id', $sellerId)->get();
    }

    public function createMenyProductReview(int $sellerId, array $productReviews)
    {
    	foreach ($productReviews as $productReview) {
    		$this->createProductReview($productReview);
    	}

    	return true;
    }

    public function createProductReview(array $fields)
    {
		return ProductReview::create($fields);
    }

    public function deleteProductReview(int $sellerId)
    {
    	ProductReview::where('seller_id', $sellerId)->delete();
    }

    public function getParamsErrorproductDiscount(Seller $seller)
    {
        return [
            'status' => 200,
            'product_discount' => $seller->total_product_discount,
            'seller_id' => $seller->seller_id,
            'action' => 'tool_reshow_info'
        ];
    }

    public function getTotalRecords(Seller $seller, string $reviewTypeString)
    {
        switch ($reviewTypeString) {
            case 'positive':
                return $seller->seller_reviews_positive;
            case 'neutral':
                return $seller->seller_reviews_neutral;
            case 'negative':
                return $seller->seller_reviews_negative;
            default:
                return $seller->seller_reviews_positive;
        }
    }

    public function getProductReviewsData(int $sellerId, $productReviews)
    {
        $html = new simple_html_dom();
        $productReviewsData = [];

        foreach ($productReviews->data as $productReviewHtml) {
            $html->load($productReviewHtml);

            foreach ( $html->find('.c-review__title') as $one ) {
                $review__title = $one->plaintext;
                if( $review__title ) continue;
            }
            foreach ( $html->find('.c-review__date') as $one ) {
                $review__date = $one->plaintext;
                if( $review__date ) continue;
            }
            foreach ( $html->find('.c-review__name-text') as $one ) {
                $review__name = $one->plaintext;
                if( $review__name ) continue;
            }
            foreach ( $html->find('.c-review__comment') as $one ) {
                $review__comment = $one->plaintext;
                if( $review__comment ) continue;
            }
            foreach ( $html->find('.c-review__product-title') as $one ) {
                $product__title = $one->plaintext;
                if( $product__title ) continue;
            }
            foreach ( $html->find('.c-review__image') as $one ) {
                $product__img = $one->getAttribute('src');
                if( $product__img ) continue;
            }
            foreach ( $html->find('.c-review__product-thumbnail a') as $one ) {
                $product__link = $one->getAttribute('href');
                if( $product__link ) continue;
            }

            foreach ( $html->find('.c-rating-stars__active') as $one ) {
                $reivew_rating = $one->getAttribute('style');
                if( $reivew_rating ) continue;
            }

            if ($reivew_rating) {
                preg_match('!\d+!', $reivew_rating, $matches);
                $reivew_rating_percent = $matches[0]; 
                $reivew_rating = ceil($reivew_rating_percent/20);
            }

            $productReviewsData[] = [
                'title' => $review__title,
                'seller_id' => $sellerId,
                'name' => $review__name,
                'text' => $review__comment,
                'rating' => $reivew_rating,
                'pro_image_url' => $product__img,
                'pro_title' => $product__title,
                'pro_url' => $product__link
            ];
        }

        return $productReviewsData;
    }

    public function getProductReviewUrl(Seller $seller, int $reviewPage)
    {
        $url = 'https://www.lazada.sg' . $seller->product_reviews_link;
        $clearUrl = str_replace('/products','/product-reviews', $url);

        return $clearUrl . '/?page=' . $reviewPage . '&size=10';
    }

    public function getContantJson(string $url)
    {
        return json_decode(file_get_contents($url)) ?? false;
    }

    public function getSellerReviewsArray(int $sellerId, $html, string $reviewTypeString)
    {
        $sellerReviews = [];

        foreach($html->find('.seller-review-item') as $item) {
             foreach ($item->find('.seller-review-item__author') as $one) {
                $name = $one->plaintext ?? null;
             }

             foreach ($item->find('.seller-review-item__content') as $one) {
                $text = $one->plaintext ?? null;
             }

             foreach ($item->find('.seller-review-item__time') as $one) {
                $createdAtString = $one->plaintext ?? null;
             }
            
             $sellerReviews[] = [
                'seller_id' => $sellerId,
                'name' => $name,
                'text' => $text,
                'review_type' => $reviewTypeString,
                'created_at_string' => $createdAtString,
             ];
        }

        return $sellerReviews;
    }
}
