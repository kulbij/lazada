<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $table = 'product_reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id',
        'title',
        'text',
        'name',
        'rating',
        'pro_image_url',
        'pro_title',
        'pro_url'
    ];
}
