<?php

namespace App\Http\Controllers;

use App\Http\LazadaApi\LazadaAPI;
use Illuminate\Http\Request;

class LazadaApiController extends Controller
{

    /**
     * @return $this
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function ajaxGetData(Request $request)
    {
        $config = [
            'userId' => $request->get('email'),
            'key' => $request->get('key'),
        ];

        try {
            $data = [
                'metrics' => $this->appLazada($config)->getMetrics(),
                'payoutstatus' => $this->appLazada($config)->getPayoutStatus(),
                'statistic' => $this->appLazada($config)->getStatistic(),
            ];
        }
        catch (\Exception $e) {
            return response(['error' => 1]);
        }


        return view('data.lazadaback')->with(['data' => $data]);
    }

    /**
     * @param $config
     * @return LazadaAPI
     */
    private function appLazada($config)
    {
        return new LazadaAPI($config);
    }
}
