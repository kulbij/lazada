<?php

namespace App\Http\Controllers;

use App\Traits\CurlTrait;
use Illuminate\Http\Request;
use App\Traits\LazadaFrontTrait;

class LazadaFrontController extends Controller
{
    use CurlTrait, LazadaFrontTrait;

    public $reviewTypes = [
        'positive',
        'neutral',
        'negative'
    ];

    /**
     * @return $this
     */
    public function index()
    {
        return view('index');
    }

    public function showSellerReviews(int $sellerId)
    {
        $sellerReviews = $this->getSellerReviews($sellerId);

        return view('pages.lazadafront.seller_reviews', [
            'seller_reviews' => $sellerReviews
        ]);
    }

    public function showProductReviews(int $sellerId)
    {
        $productReviews = $this->getProductSellers($sellerId);

        return view('pages.lazadafront.product_reviews', [
            'product_reviews' => $productReviews
        ]);
    }

    public function ajaxGetData(Request $request)
    {
        $url = $request->post('url');

        $getSeller = $this->toolStart($url);

        return response()->json([
            'view' => $this->getView($getSeller->seller),
            'seller' => $getSeller->seller->toArray(),
            'action' => $getSeller->action,
            'status' => $getSeller->status
        ]);
    }

    public function getView($seller)
    {
        return view('partials.ajax.lazadafront.fields_partial', [
            'seller' => $seller
        ])->render();   
    }

    public function createDataInsert(int $sellerId, $data)
    {
        return [
            'seller_id' => $sellerId, 
            'seller_url' => $data->url,
            'price_min' => $data->unit . ' ' . $data->priceMmin,
            'price_max' => $data->unit . ' ' . $data->priceMmax,
            'name' => $data->sellerInfo->seller->name,
            'rate' => $data->sellerInfo->seller->rate,
            'top_rated' => $data->sellerInfo->seller->top_rated,
            'categories' => implode(', ', $data->categories),
            'deliveries' => implode(', ', $data->deliveries),
            'category' => $data->sellerInfo->seller->category->name,
            'brands' => implode(', ', $data->brands),
            'size' => $data->sellerInfo->seller->size,
            'location' => $data->sellerInfo->seller->location, 
            'time_on_lazada' => $data->sellerInfo->seller->time_on_lazada->months,
            'shipped_on_time_seller_rate' => $data->sellerInfo->seller->shipped_on_time->seller_rate,
            'shipped_on_time_average_rate' => $data->sellerInfo->seller->shipped_on_time->average_rate,
            'seller_reviews_positive' => $data->sellerInfo->seller->seller_reviews->positive->total,
            'seller_reviews_neutral' => $data->sellerInfo->seller->seller_reviews->neutral->total,
            'seller_reviews_negative' => $data->sellerInfo->seller->seller_reviews->negative->total,
            'seller_reviews_link' => $data->sellerInfo->seller->seller_reviews->link,
            'product_reviews_link' => $data->sellerInfo->seller->product_reviews->reviews_page_link,
            'product_reviews_total' => $data->sellerInfo->seller->product_reviews->total,
            'positive_seller_rating' => @$data->sellerInfo->seller->positive_seller_rating, 
            'positive_seller_rating_grade' => @$data->sellerInfo->seller->positive_seller_rating_grade
        ];
    }

    function toolStart(string $url)
    {
        $html = $this->curlMethod($url);

        if (!$html) {
            return false;
        }

        $sellerId = $this->getSellerId($html);
        $jsons = $this->getJsons($html);

        $unit = $this->getUnit($jsons);
            
        if (!$sellerId) {
            return false;
        }

        $categories = $this->getCategories($html);
        $brands = $this->getBrands($html);
        $priceMmin = $this->getPriceMin($html);
        $priceMmax = $this->getPriceMax($html);
        $deliveries = $this->getDeliveries($html);

        if ($sellerId){
            $sellerId = str_replace('seller-','', $sellerId);
            $sellerInfo = file_get_contents('http://seller-transparency-api.lazada.sg/v1/seller/transparency?platform=desktop&lang=en&seller_id='.$sellerId);
            $sellerInfo = json_decode($sellerInfo);

            if ($sellerInfo) {
                $dataInsert = $this->createDataInsert($sellerId, (object) [
                    'url' => $url,
                    'unit' => $unit,
                    'categories' => $categories,
                    'brands' => $brands,
                    'priceMmin' => $priceMmin,
                    'priceMmax' => $priceMmax,
                    'deliveries' => $deliveries,
                    'sellerInfo' => $sellerInfo
                ]);

                return (object) [
                    'seller' => $this->processSeller($dataInsert, $sellerId),
                    'status' => 200,
                    'action' => 'tool_get_reviews'
                ];
            }

            return (object) [
                'seller' => $this->getSeller($sellerInfo),
                'status' => 200,
                'action' => 'tool_get_reviews'
            ];
        }

        return (object) [
            'status' => 404
        ];
    }

    public function ajaxGetSellerReviews(Request $request)
    {
        $data = $this->getReviews($request);

        return response()->json($data);
    }

    public function getReviews($request)
    {
        $sellerId = $request->post('seller_id');
        $reviewPage = $request->post('review_page') ?? 1;
        $reviewType = $request->post('review_type') ?? 0;

        $seller = $this->getSeller($sellerId);

        if (!$seller) {
            return [
                'status' => 404
            ];
        }
        
        if (isset($this->reviewTypes[$reviewType]) ) {
            $reviewTypeString = $this->reviewTypes[$reviewType];
            $totalRecords = $this->getTotalRecords($seller, $reviewTypeString);
            $totalPage = ceil($totalRecords  / 10);
            $totalPage = (int) $totalPage;
            $reviewType = (int) $reviewType;

            $sellerReviewUrl = 'https://www.lazada.sg' . $seller->seller_reviews_link . '/reviews/?type=' . $reviewTypeString . '&p=' . $reviewPage;

            $html = $this->curlMethod($sellerReviewUrl);
            if ($html) {
                $sellerReviews = $this->getSellerReviewsArray($sellerId, $html, $reviewTypeString);
                
                if ($reviewPage == 1) {
                    $this->deleteSellerReview($sellerId);
                }

                if (sizeof($sellerReviews) > 0) {
                    $this->createMenySellerReview($sellerId, $sellerReviews);
                }

                $data = [
                    'status' => 200,
                    'seller_id' => $sellerId
                ];
            }
            
            if ((int) $reviewPage < $totalPage) {
                $data['review_page'] = $reviewPage + 1;
                $data['review_type'] = $reviewType;
            }

            if ((int)$reviewPage == $totalPage) {
                return [
                    'status' => 200,
                    'action' => 'tool_get_product_reviews',
                    'seller_id' => $sellerId
                ];
            }

            if ($reviewType == 2 || $totalPage == 0) {
                $data['review_page'] = 1;
                $data['review_type'] = 3;    
            }

            $data['action'] = 'tool_get_reviews';

            return $data;
        }

        return [
            'status' => 200,
            'action' => 'tool_get_product_reviews',
            'seller_id' => $sellerId
        ];
    }

    public function ajaxGetProductReviews(Request $request)
    {
        $data = $this->getProductReviews($request);

        return response()->json($data);
    }

    function getProductReviews($request)
    {
        $sellerId = $request->post('seller_id');
        $reviewPage = $request->post('review_page') ?? 1;
        
        $seller = $this->getSeller($sellerId);

        if (!$seller) { 
            return [
                'status' => 404
            ];
        }

        $sellerProductReviewUrl = $this->getProductReviewUrl($seller, $reviewPage);
        $productReviews = $this->getContantJson($sellerProductReviewUrl);

        if (!$productReviews && sizeof($productReviews->data) > 0) {
            $productReviewsData = $this->getProductReviewsData($sellerId, $productReviews);

            if ($reviewPage == 1) {
                $this->deleteProductReview($sellerId);
            }

            if (sizeof($productReviewsData) > 0) {
                $this->createMenyProductReview($sellerId, $productReviewsData);
            }

            return [
              'status' => 200,
              'review_page' => ((int) $reviewPage + 1),
              'seller_id' => $sellerId,
              'action' => 'tool_get_product_reviews'
            ];
        }

        $this->updateSeller($this->getSeller($sellerId), [
            'total_product' => 0,
            'total_product_discount' => 0
        ]);

        return [
          'status' => 200,
          'seller_id' => $sellerId,
          'action' => 'tool_get_product_discount'
        ];
    }

    public function ajaxGetProductDiscount(Request $request)
    {
        $data = $this->getProductDiscount($request);

        return response()->json($data);
    }

    function getProductDiscount($request)
    {
        $sellerId = $request->post('seller_id');
        $productPage = $request->post('product_page') ?? 1;
        
        $seller = $this->getSeller($sellerId);

        if (!$seller) {
            return [
                'status' => 404
            ];
        }

        $sellerUrl = str_replace('//?dir','/?dir', $seller->seller_url);

        $html = $this->curlMethod($sellerUrl);

        if (!$html) {
            return $this->getParamsErrorproductDiscount($seller);
        }

        $allPageObjs = [];
        foreach ($html->find('.c-paging__link') as $link) {
            $allPageObjs[] = $link->plaintext ?? null;
        }

        $maxPageAll = array_filter($allPageObjs);
        $maxPageAll = end($allPageObjs) ?? 1;

        $sellerUrlProducts = $seller->seller_url . '/?itemperpage=30&page=' . $maxPageAll;

        $html = $this->curlMethod($sellerUrlProducts);

        if (!$html) {
            return $this->getParamsErrorproductDiscount($seller);
        }

        $productObjs = $html->find('.c-product-card__price-final');

        if (sizeof($productObjs) > 0) {
            $totalProductAll = (36 *  (int) $maxPageAll);
            $totalProductAll = ((int) $totalProductAll - 36) + sizeof($productObjs);
        }

        $sellerUrlDiscount = $seller->seller_url.'/?itemperpage=30&sort=discountspecial&page=' . $productPage;

        $html = $this->curlMethod($sellerUrlDiscount);

        if (!$html) {
            return $this->getParamsErrorproductDiscount($seller);
        }

        $productDiscount = $html->find('.c-product-card__old-price');

        $countProductDiscount = sizeof($productDiscount);

        if ($countProductDiscount > 0 ) {
            $this->updateSeller($this->getSeller($sellerId), [
                'total_product_discount' => $countProductDiscount + $seller->total_product_discount
            ]);

            return [
                'status' => 200,
                'product_page' => (int) $productPage + 1,
                'seller_id' => $sellerId,
                'action' => 'tool_get_product_discount'
            ];

        }

        $this->updateSeller($this->getSeller($sellerId), [
            'total_product_non_discount' => $totalProductAll - $seller->total_product_discount
        ]);

        return [
            'status' => 200,
            'product_discount' => $seller->total_product_discount,
            'seller_id' => $sellerId,
            'action' => 'tool_reshow_info',
            'product_non_discount' => $totalProductAll - $seller->total_product_discount
        ];
    }
}
