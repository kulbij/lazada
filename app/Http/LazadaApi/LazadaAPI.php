<?php

namespace App\Http\LazadaApi;
use \DateTime;

/**
 * Class LazadaAPI
 * @package App\Http\LazadaApi
 */
class LazadaAPI
{
	const DELIVERY_TYPE = 'dropship';

	const LAZADA_URL = "https://api.sellercenter.lazada.sg/";

	public $parameters;

	/**
	 * Api KEY
	 * @var
	 */
	protected $apiKey;

	/**
	 * User ID
	 * @var
	 */
	protected $userId;


	/**
	 * LazadaAPI constructor.
	 * @param array $config
	 * @throws \Exception
	 */
	public function __construct(array $config)
	{
		$this->parameters = ['Format' => 'JSON', 'Version' => '1.0', 'UserID' => $config['userId']];

		if(!$config['key'] || !$config['userId']) {
			throw new \Exception('No key or user id.');
		}

		$this->setApiKey($config['key']);
		$this->setUserID($config['userId']);
	}

	/**
	 * @param array $params
	 * @return mixed
	 */
	public function getOrders(array $params = [])
	{
		$parameters = array_merge($params, $this->parameters);
		$parameters['Action'] = 'GetOrders';

		$data = $this->execute($parameters);

		return $data;
	}

    /**
     * @param array $params
     * @return mixed
     */
    public function getBrands(array $params)
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetBrands';

        $data = $this->execute($parameters);

        return $data;
    }

    public function getProducts(array $params = [])
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetProducts';

        $data = $this->execute($parameters);

        return $data;
    }

    public function getFailerOrderReasons(array $params = [])
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetFailureReasons';

        $data = $this->execute($parameters);

        return json_decode($data, true);
    }


	/**
	 * @param $parameters
	 * @return mixed
	 */
	protected function execute($parameters)
	{
		$now = new DateTime();
		$encoded = [];
		$parameters['Timestamp'] = $now->format(DateTime::ISO8601);

		ksort($parameters);

		foreach ($parameters as $name => $value) {
			$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
		}

		$concatenated = implode('&', $encoded);
		$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $this->apiKey, false));
		$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
		$this->parameters['Signature'] = rawurlencode(hash_hmac('sha256', $queryString, $this->apiKey, false));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::LAZADA_URL."?".$queryString);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	/**
	 * @param $params
	 * @return mixed
	 */
	public function getOrderItems($params)
	{
		$parameters = array_merge($params, $this->parameters);
		$parameters['Action'] = 'GetOrderItems';

		$data = $this->execute($parameters);

		return $data;
	}

    public function getMetrics(array $params = [])
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetMetrics';

        $data = $this->execute($parameters);

        return json_decode($data, true)['SuccessResponse']['Body'];
    }

    public function getPayoutStatus(array $params = [])
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetPayoutStatus';

        $data = $this->execute($parameters);

        return json_decode($data, true)['SuccessResponse']['Body']['PayoutStatus'];
    }

    public function getStatistic(array $params = [])
    {
        $parameters = array_merge($params, $this->parameters);
        $parameters['Action'] = 'GetStatistics';

        $data = $this->execute($parameters);

        return json_decode($data, true)['SuccessResponse']['Body'];
    }

	/**
	 * @param array $OrderItemIds
	 * @param $shippingProvider
	 * @return mixed
	 */
	public function setStatusToPackedByMarketplace(array $OrderItemIds, $shippingProvider)
	{
		$parameters = $this->parameters;
		$parameters['Action'] = 'SetStatusToPackedByMarketplace';
		$parameters['OrderItemIds'] = $OrderItemIds;
		$parameters['DeliveryType'] = self::DELIVERY_TYPE;
		$parameters['ShippingProvider'] = $shippingProvider;

		$data = $this->execute($parameters);

		return $data;
	}

	/**
	 * @param array $OrderItemIds
	 * @param $shippingProvider
	 * @param $trackingNumber
	 * @return mixed
	 */
	public function setStatusToReadyToShip(array $OrderItemIds, $shippingProvider, $trackingNumber)
	{
		$parameters = $this->parameters;
		$parameters['Action'] = 'SetStatusToReadyToShip';
		$parameters['OrderItemIds'] = $OrderItemIds;
		$parameters['DeliveryType'] = self::DELIVERY_TYPE;
		$parameters['ShippingProvider'] = $shippingProvider;
		$parameters['TrackingNumber'] = $trackingNumber;

		$data = $this->execute($parameters);

		return $data;
	}

	/**
	 * @param array $params
	 * @return string
	 */
	public function getShipmentProviders($params = [])
	{
		$nameShipmProvider = [];
		$parameters = array_merge($params, $this->parameters);
		$parameters['Action'] = 'GetShipmentProviders';

		$data = $this->execute($parameters);

		$dataToJson = json_decode($data, true);

		foreach ($dataToJson['SuccessResponse']['Body']['ShipmentProviders'] as $provider) {
            $nameShipmProvider[] = $provider['Name'];
        }

        return $nameShipmProvider;
	}

	/**
	 * @param $key
	 */
	private function setApiKey($key)
	{
		$this->apiKey = $key;
	}

	/**
	 * @param $key
	 */
	private function setUserID($key)
	{
		$this->userId = $key;
	}

}