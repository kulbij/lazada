<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerReview extends Model
{
    protected $table = 'seller_reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id',
        'name',
        'text',
        'created_at_string',
        'review_type'
    ];

    public function user()
    {
        return $this->all();
    }
}
